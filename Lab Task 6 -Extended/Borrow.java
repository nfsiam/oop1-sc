import java.lang.*;

public class Borrow 
{
	Book book;
	Date borrowDate;
	Date returnDate;

	public Borrow(){}

	public Borrow(Book book, Date borrowDate)
	{
		this.book=book;
		this.borrowDate=borrowDate;
		this.returnDate=borrowDate;
	}

	public void showDetails()
	{
		book.showDetails();
		System.out.println("Borrowing Date :"+borrowDate.day+"-"+borrowDate.month + "-" +borrowDate.year);
		System.out.println("Rturning Date :"+(returnDate.day+7)+"-"+borrowDate.month + "-" +borrowDate.year);
		System.out.println();
	}
}