import java.lang.*;

public class Start
{
	public static void main(String[] args)
	{
		Library l1 = new Library("l1","first block",5);
		

		Book b1 = new Book ("001", "Abc" , "NF Siam" , 2001 , 10);
		Book b2 = new Book ("002", "Def" , "NF Siam" , 2002 , 20);
		Book b3 = new Book ("003", "Ijk" , "NF Siam" , 2003 , 30);
		


		l1.addBook(b1);
		l1.addBook(b2);
		l1.addBook(b3);
		

		System.out.println("\n#Showing details of l1 after adding b1,b2,b3 books\n");
		l1.showDetails();


		Date d1 = new Date(01,01,2017);
		Date d2 = new Date(01,02,2018);

		Borrow br1 = new Borrow(b1,d1);
		Borrow br2 = new Borrow(b2,d2);

		Student s1 = new Student("Siam,Nafiz Fuad","17-*****-1",3);

		s1.borrowBook(br1);
		s1.borrowBook(br2);


		System.out.println("\n#Showing details of s1 after borrowing br1 and br2\n");
		s1.showDetails();

		

		System.out.println("\n#Showing details of l1 after borrowoing br1 and br2\n");
		l1.showDetails();

		
		System.out.println("\nCount of total books in the libarary after borrowing br1 and br2: "+l1.countTotalBooksIntheLibrary()+"\n\n" );

		
		s1.returnBook(br1);

		System.out.println("\n#Showing details of s1 after returning the book of br1\n");
		s1.showDetails();

		
		System.out.println("\n#Showing details of l1 after returning the book of br1\n");
		l1.showDetails();


		l1.removeBook(b2);

		
		System.out.println("\n#Showing details of l1 after removing b2 book from l1\n");
		l1.showDetails();

		
	}

}