import java.lang.*;

public class Book
{
	String bookId;

	String bookTitle;

	String authorName ;

	int publicationYear;

	int availableQuantity;

	public Book( ){}
	
	public Book(String bookId, String bookTitle,String authorName, int publicationYear,int availableQuantity)
	{
		this.bookId=bookId;
		this.bookTitle=bookTitle;
		this.authorName=authorName;
		this.publicationYear=publicationYear;
		this.availableQuantity=availableQuantity;
	}
	
	public void showDetails( )
	{
		System.out.println("Book ID : "+bookId);
		System.out.println("Book Title : "+bookTitle);
		System.out.println("Author Name : "+authorName);
		System.out.println("Publication Year : "+publicationYear);
		System.out.println("Available Quantity : "+availableQuantity);
		System.out.println();		
	}
}
