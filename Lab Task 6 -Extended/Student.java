import java.lang.*;

public class Student
{
	String name;
	String id;
	Borrow [ ]borrowedBooks;

	Student( ){}
	Student(String name, String id, int sizeOfArray)
	{
		this.name=name;
		this.id=id;
		borrowedBooks=new Borrow[sizeOfArray];
	}
	
	public void borrowBook(Borrow b)
	{
		for(int i=0;i<borrowedBooks.length;i++)
		{
			if(borrowedBooks[i]==null)
			{
				borrowedBooks[i]=b;
				b.book.availableQuantity--;
				break;
			}
		}
	}


	public void returnBook(Borrow b)
	{
		
		for(int i=0; i<borrowedBooks.length;i++)
		{
			if(borrowedBooks[i]==b)
			{
				for(int j=i;j<(borrowedBooks.length)-1;j++)
				{
					borrowedBooks[j]=borrowedBooks[j+1];
				}
				b.book.availableQuantity++;
			}
		}
	}



	public void showDetails( )
	{
		System.out.println(name);
		System.out.println(id);
		System.out.println();


		for(int i=0; i<borrowedBooks.length; i++)
		{
			if(borrowedBooks[i]!=null)
			{
				borrowedBooks[i].showDetails();
			}
		}
	}
	

	public double calculateFine( ){return 0;}



}