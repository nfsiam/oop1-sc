import java.lang.*;

public class  Library
{
	String libraryName;
	String location;
	Book books[ ];

	public Library( ){}
	public Library(String libraryName, String location, int sizeOfArray)
	{
		this.libraryName=libraryName;
		this.location=location;
		books = new Book[sizeOfArray];
	}
	

	public void showDetails( )
	{
		System.out.println("Library name: "+libraryName);
		System.out.println("Library Location: "+location);
		System.out.println();
		
		for(int i=0; i<books.length; i++)
		{
			if(books[i]!=null)
			{
				books[i].showDetails();
			}
		}
		System.out.println("\n");
	}
	

	public void addBook(Book b)
	{
		for(int i=0;i<10;i++)
		{
			if(books[i]==null)
			{
				books[i]=b;
				break;
			}
		}
	}
	
	public void removeBook(Book b)
	{
		for(int i=0; i<books.length;i++)
		{
			if(books[i]==b)
			{
				for(int j=i;j<(books.length)-1;j++)
				{
					books[j]=books[j+1];
				}
			}
		}
	}


	public int countTotalBooksIntheLibrary( )
	{
		int c=0;
		for (int i=0;i<books.length;i++)
		{
			if(books[i]!=null)
			{
				c=c+books[i].availableQuantity;
			}
		}
		return c;
	}
}