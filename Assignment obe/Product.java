import java.lang.*;

public class Product
{
	int productId;
	String productName;
	double price;
	int availableQuantity;

	public Product(){}

	public Product(int productId, String productName, double price, int availableQuantity)
	{
		this.productId=productId;
		this.productName=productName;
		this.price=price;
		this.availableQuantity=availableQuantity;
	}

	public void setProductId(int productId)
	{
		this.productId=productId;
	}
	public void setProductName(String productName)
	{
		this.productName=productName;
	}
	public void setPrice(double price)
	{
		this.price=price;
	}
	public void setAvailableQuantity(int availableQuantity)
	{
		this.price=price;
	}

	public int setProductId()
	{
		return productId;
	}
	public String setProductName()
	{
		return productName;
	}
	public double setPrice()
	{
		return price;
	}
	public int setAvailableQuantity()
	{
		return availableQuantity;
	}

	public void buyProducts(int quantity)
	{
		this.availableQuantity=this.availableQuantity+quantity;
	}

	public void sellProducts(int quantity)
	{
		this.availableQuantity=this.availableQuantity-quantity;
	}
}