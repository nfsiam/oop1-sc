import java.lang.*;

public class Start
{
	public static void main(String[] args)
	{
		//1
		Shop s1 = new Shop("Dokan", 1000);
		
		//2
		Product p1 = new Product(1,"product1",100.00,10);
		Product p2 = new Product(2,"product2",200.00,20); 
		Product p3 = new Product(3,"product3",300.00,30); 
		Product p4 = new Product(4,"product4",400.00,40); 

		//3
		s1.addProduct(p1);
		s1.addProduct(p2);
		s1.addProduct(p3);

		//4
		System.out.println("Shop name: "+s1.getShopName());

		for(int i=0;i<s1.products.length;i++)
		{
			if(s1.products[i]!=null)
			{
				System.out.println("Product ID: "+s1.products[i].productId);
				System.out.println("Product Name: "+s1.products[i].productName);
				System.out.println("Product Price: "+s1.products[i].price);
				System.out.println("Available Quantity: "+s1.products[i].availableQuantity);
				System.out.println();
			}
		}

		//5
		for(int i = 0; i<s1.products.length; i++)
		{
			if(s1.products[i]==p2)
			{
				s1.products[i].buyProducts(5);
			}
		}

		//6
		for(int i = 0; i<s1.products.length; i++)
		{
			if(s1.products[i]==p3)
			{
				s1.products[i].sellProducts(5);
			}
		}

		//7
		for(int i=0;i<s1.products.length;i++)
		{
			if(s1.products[i]!=null)
			{
				System.out.println("Product ID: "+s1.products[i].productId);
				System.out.println("Product Name: "+s1.products[i].productName);
				System.out.println("Product Price: "+s1.products[i].price);
				System.out.println("Available Quantity: "+s1.products[i].availableQuantity);
				System.out.println();
			}
		}

		//8
		s1.removeProduct(p1);
		s1.addProduct(p4);

		//9
		for(int i=0;i<s1.products.length;i++)
		{
			if(s1.products[i]!=null)
			{
				System.out.println("Product ID: "+s1.products[i].productId);
				System.out.println("Product Name: "+s1.products[i].productName);
				System.out.println("Product Price: "+s1.products[i].price);
				System.out.println("Available Quantity: "+s1.products[i].availableQuantity);
				System.out.println();
			}
		}

	}
}